import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'

export default class UsersController {
  public async index () {
    return await User.all()
  }

  public async get ({ params }: HttpContextContract) {
    let user_id: String = params.id
    return await User.find(user_id)
  }

  public async create ({ request }: HttpContextContract) {
    let user = request.only(['name'])
    return await User.create(user)
  }
}
